import Foundation
import Connect

@objc(Finicity) class Finicity : CDVPlugin, UIAdaptivePresentationControllerDelegate {
    
    var connectViewController: ConnectViewController!
    var connectNavController: UINavigationController!
    var localCommand: CDVInvokedUrlCommand!
    
    @objc(connect:)
    func connect(_ command: CDVInvokedUrlCommand) {
        self.localCommand = command
        
        if let connectUrl = command.arguments[0] as? String {
            let config = ConnectViewConfig(connectUrl: connectUrl, loaded: self.connectViewLoaded, done: self.connectViewDone, cancel: self.connectViewCancelled, error: self.connectViewError, route: self.connectViewRoute, userEvent: self.connectViewUserEvent)
            self.connectViewController = ConnectViewController()
            self.connectViewController.load(config: config)
        } else {
            let pluginResult: CDVPluginResult
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "no connect url provided.");
            self.commandDelegate.send(pluginResult, callbackId:self.localCommand.callbackId)
        }
    }
    
    func connectViewLoaded() {
        //print("connectViewController loaded")
		self.connectNavController = UINavigationController(rootViewController: self.connectViewController)
		self.connectNavController.modalPresentationStyle = .fullScreen
		self.viewController.present(self.connectNavController, animated: false)

		//this seems to be right code as it is in Finicity's sample: https://github.com/Finicity/connect-ios-sdk/blob/main/ConnectWrapper/ConnectWrapper/ViewController.swift
		//however unable to compile this code through IONIC's Appflow. Errors:
		//'isModalInPresentation' is only available in iOS 13.0 or newer
		//'automatic' is only available in iOS 13.0 or newer
		//print("connectViewController loaded")
        //self.connectNavController = UINavigationController(rootViewController: self.connectViewController)
        //self.connectNavController.modalPresentationStyle = .automatic
        //self.connectNavController.isModalInPresentation = true
        //self.connectNavController.presentationController?.delegate = self
        //self.viewController.present(self.connectNavController, animated: true)
    }
    
    func connectViewDone(_ data: NSDictionary?) {
        print("connectViewController done")
        print(data?.debugDescription ?? "no data in callback")
        //Needed to trigger deallocation of ConnectViewController
        self.connectViewController = nil
        self.connectNavController = nil
        
        let pluginResult: CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Success");
        self.commandDelegate.send(pluginResult, callbackId:self.localCommand.callbackId)
    }
    
    func connectViewCancelled() {
        print("connectViewController cancel")
        self.viewController.dismiss(animated: true, completion: nil)
        
        // Needed to trigger deallocation of ConnectViewController
        self.viewController.dismiss(animated: true, completion: nil)
        self.connectViewController = nil
        self.connectNavController = nil
        
        let pluginResult: CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Cancelled");
        self.commandDelegate.send(pluginResult, callbackId:self.localCommand.callbackId)
    }
    
    func connectViewError(_ data: NSDictionary?) {
        print("connectViewController error")
        print(data?.debugDescription ?? "no data in callback")
        self.viewController.dismiss(animated: true, completion: nil)
        // Needed to trigger deallocation of ConnectViewController
        self.connectViewController = nil
        self.connectNavController = nil
        
        let pluginResult: CDVPluginResult
        pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Error");
        self.commandDelegate.send(pluginResult, callbackId:self.localCommand.callbackId)
    }
    
    func connectViewRoute(_ data: NSDictionary?) {
        print("connectViewController route")
        print(data?.debugDescription ?? "no data in callback")
    }
    
    func connectViewUserEvent(_ data: NSDictionary?) {
        print("connectViewController user")
        print(data?.debugDescription ?? "no data in callback")
    }
}