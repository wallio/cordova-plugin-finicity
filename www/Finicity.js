module.exports = function (connectUrl, success, error) {
    cordova.exec(success, error, "Finicity", "connect", [connectUrl]);
};